# Fire and Water

## :pencil: Opis igre :
---

Igrači u paru prolaze kroz različite nivoe. Njihov cilj je da, izbegavajući prepreke, sakupe sve dijamante svoje boje i tako otključaju vrata. Na sledeći nivo se prelazi kada oba igrača stignu do svojih vrata. Ukoliko neki od njih ne uspe da savlada prepreku, oba igrača se vraćaju na početak.

## Programski jezik :
[![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-blue)](https://www.cplusplus.com/) <br>
[![qt6](https://img.shields.io/badge/Framework-Qt6-green)](https://doc.qt.io/qt-6/) <br>

## Okruzenje :
[![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-green)](https://www.qt.io/product/development-tools)

## :books: Koriscene biblioteke : 
* Qt >= 6.2.2

## :hammer: Instalacija :
**Qt** i **Qt Creator** se mogu preuzeti na [ovom](https://www.qt.io/download) linku. 

## :wrench: Preuzimanje i pokretanje :
1. U terminalu se pozicionirati u zeljeni direktorijum
2. Klonirati repozitorijum komandom: $ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/08-fire-and-water.git
3. Otvoriti okruzenje **Qt Creator** i u njemu otvoriti .pro fajl
4. Pritisnuti dugme **Run** (Ctrl+R) u donjem levom uglu ekrana

## :movie_camera: Link do demo snimka :
https://drive.google.com/file/d/1DsxBMNUiYlDWrBcwUJh9Rwcg-FCweOaQ/view?usp=sharing

## :busts_in_silhouette: Developers
<ul>
    <li><a href="https://gitlab.com/smajli99">Luka Radenković 59/2018</a></li>
    <li><a href="https://gitlab.com/Tihomir-99">Tihomir Stojković 283/2018</a></li>
    <li><a href="https://gitlab.com/simic_mileva18">Mileva Simić 156/2019</a></li>
    <li><a href="https://gitlab.com/pejovicnina">Nikolina Pejović 172/2019</a></li>
</ul>

